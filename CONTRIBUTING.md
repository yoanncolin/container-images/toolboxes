Ansible guests containers
=========================

Dependencies
------------

Install and configure :

- Ansible
- Molecule
- molecule-podman
- Podman
- Toolbox

Developments
------------

### Rules

See https://containertoolbx.org/distros/.

### Supported distros

The supported Linux distributions are listed in `vars/images.yml`.

To add one, please, do not edit Dockerfiles, `verify.yml` or `README.md`
by hand.

Add informations in `vars/images.yml` and build source files with Ansible :

```sh
ansible-playbook gen-files.yml
```

Build
-----

```sh
molecule create
```

Build just an image :

```sh
image=debian-12
podman build -t localhost/$image -f Containerfile-$image .
```

Test
----

Test each container image with Molecule :

```sh
molecule verify
```

List available images :

```sh
toolbox list
```

Test just one image by hand, for example with ArchLinux :

```sh
toolbox create -i <image-name> test
toolbox enter test
# Do your tests
toolbox rm -f test
```

Deploy
------

```sh
ansible-playbook push.yml
```

    **Note**

    Docker images are built and tagged with the real names, but the push to
    DockerHub is done only if `GWERLAS_DOCKER_USER` and `GWERLAS_DOCKER_PASS`
    environment variables are set.
