[ "${BASH_VERSION:-}" != "" ] || [ "${ZSH_VERSION:-}" != "" ] || return 0

if [ -f /run/.containerenv ] \
   && [ -f /run/.toolboxenv ]; then
    source /etc/os-release
    [ "${BASH_VERSION:-}" != "" ] && PS1=$(printf "\[\033[35m\]⬢\[\033[0m\]%s" "[$ID]\\$ ")
    [ "${ZSH_VERSION:-}" != "" ] && PS1=$(printf "\033[35m⬢\033[0m%s" "[$ID]%~%# ")
fi
